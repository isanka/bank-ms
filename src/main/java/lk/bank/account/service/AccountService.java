package lk.bank.account.service;

import lk.bank.account.model.Account;

public interface AccountService {

	public Account getAccount(String accountId);
}
