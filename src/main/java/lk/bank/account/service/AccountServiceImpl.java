package lk.bank.account.service;

import org.springframework.stereotype.Service;

import lk.bank.account.model.Account;

@Service
public class AccountServiceImpl implements AccountService {

	@Override
	public Account getAccount(String accountId) {

		Account account = new Account();
		account.setAccountId(accountId);
		account.setName("Test_account_name");
		account.setCity("Nuwara Eliya");
		account.setAddress("Hawa Eliya");
	
		return account;
	}

}
