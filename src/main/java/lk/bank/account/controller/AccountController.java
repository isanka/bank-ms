package lk.bank.account.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import lk.bank.account.domain.ApiResponse;
import lk.bank.account.model.Account;
import lk.bank.account.service.AccountService;

import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/bank")
public class AccountController {

	@Autowired
	private AccountService accountService;

	@GetMapping("/account")
	public ResponseEntity<ApiResponse<Account>> getAccountInfo() {

		return new ApiResponse.ApiResponseBuilder<Account>().withHttpStatus(HttpStatus.OK)
				.withData(accountService.getAccount("accountId")).build();
	}

	@GetMapping("/accountInfo")
	public ResponseEntity<ApiResponse<Account>> getAccountInfoByID(@PathParam("id") String id) {

		System.out.println(id);
		return new ApiResponse.ApiResponseBuilder<Account>().withHttpStatus(HttpStatus.OK)
				.withData(accountService.getAccount(id)).build();
	}

	@GetMapping("/account/Info/{id}")
	public ResponseEntity<ApiResponse<Account>> getAccountInfoByPathVariable(@PathVariable String id) {
		return new ApiResponse.ApiResponseBuilder<Account>().withHttpStatus(HttpStatus.OK)
				.withData(accountService.getAccount(id)).build();
	}
}
