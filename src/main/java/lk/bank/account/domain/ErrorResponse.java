package lk.bank.account.domain;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "statusCode", "statusDescription" })
public class ErrorResponse {

	private int statusCode;

	private String statusDescription;

	public ErrorResponse(int statusCode, String statusDescription) {
		this.statusCode = statusCode;
		this.statusDescription = statusDescription;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	@Override
	public String toString() {
		return "ErrorResponse{" + "statusCode=" + statusCode + ", statusDescription='" + statusDescription + '\'' + '}';
	}
}